# poc-graphql

A graphql POC implementation

## Goal

The goal of this POC is to create a graphql service.

## Prerequisites

The following is required to run this POC

- NodeJS, version 14+
- PM2, installed globally (`npm -g pm2`)
- A MongoDB database w/ the following collections

## Setup

TODO


## Configuration

TODO

## Running the POC

To run the system, start PM2 with the included `ecosystem.config.js` file.

```
$ pm2 start ecosystem.config.js
```

The config file is set to watch the individual services and restart upon change to the code.

## Stopping the POC

To stop the POC and retain the services in the PM2 daemon

```
$ pm2 stop all
```

To stop the services and remove the services from the PM2 Daemon.

```
$ pm2 kill
```

The kill command is great if you want to start fresh.


## Querying the Service

TODO

## Questions? Comments?

You can reach me on teams. Remember there are no stupid questions, just stupid reasons for not asking questions.


