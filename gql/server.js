require('dotenv').config();

const express = require("express");
const cors = require('cors')
const expressJwt = require("express-jwt");
const { MongoClient } = require('mongodb');
const { ApolloServer } = require("apollo-server-express");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { applyMiddleware } = require('graphql-middleware');
const { typeDefs } = require('./type-defs');
const { resolvers } = require('./resolvers');
const { guard } = require('./guard');
const {
  AuthDataSource,
  CommentDataSource,
  IdeaDataSource,
  UserDataSource,
  VoteDataSource
} = require('./data-sources');


const client = new MongoClient(process.env.MONGO_HOST);
client.connect()

const schema = makeExecutableSchema({ typeDefs, resolvers });
const guardedSchema = applyMiddleware(schema, guard)

const server = new ApolloServer({
  schema: guardedSchema,
  cors: true,
  dataSources: () => {
    return {

      auth: new AuthDataSource(
        client
          .db(process.env.MONGO_DATABASE_DATA)
          .collection(process.env.AUTH_COLLECTION)
      ),
      comment: new CommentDataSource(
        client
          .db(process.env.MONGO_DATABASE_DATA)
          .collection(process.env.COMMENTS_COLLECTION)
      ),
      idea: new IdeaDataSource(
        client
          .db(process.env.MONGO_DATABASE_DATA)
          .collection(process.env.IDEAS_COLLECTION)
      ),
      user: new UserDataSource(
        client
          .db(process.env.MONGO_DATABASE_DATA)
          .collection(process.env.USERS_COLLECTION)
      ),
      vote: new VoteDataSource(
        client
          .db(process.env.MONGO_DATABASE_DATA)
          .collection(process.env.VOTES_COLLECTION)
      )
    }
  },
  context: ({ req }) => {
    const user = req.user || null;
    return { user };
  }
});

async function launchServer() {

  await server.start();
  const app = express();
  app.use(cors({
    origin: '*',
  }));
  app.use(
    expressJwt({
      secret: process.env.JWT_SECRET,
      algorithms: [process.env.JWT_ALGO],
      credentialsRequired: false
    })
  );
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  server.applyMiddleware({ app });
  await new Promise(resolve => app.listen({ port: process.env.SERVER_PORT }, resolve));
  console.log(`🚀 Server ready at http://localhost:${process.env.SERVER_PORT}${server.graphqlPath}`);

}
launchServer();