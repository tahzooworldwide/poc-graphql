const { gql } = require('apollo-server');

const typeDefs = gql`

  """
  Authentication response

  If successful, 
    'ok' will have a value of 'true' 
    and a bearer 'token' will be return

  If failed, 
    'ok' will have a vlaue of 'false'
    and a 'message' will convey the reason for failure
  """

  type AuthResponse {
    token: String
    message: String
    ok: Boolean!
  }
  
  type Comment {
    _id: ID!
    id: Int!
    userId: Int!
    user: User
    ideaId: Int!
    idea: Idea
    body: String!
    createdAt: Float!
  }

  input CommentInput {
    ideaId: Int!
    userId: Int!
    body: String
  }

  type Idea {
    _id: ID!
    id: Int!
    userId: Int!
    user: User
    title: String!
    body: String
    createdAt: Float!
    updatedAt: Float!
    votes: [Vote]
    voteCount: Int
    comments: [Comment]
  }

  input IdeaCreateInput {
    userId: Int!
    title: String!
    body: String
  }
  input IdeaUpdateInput {
    title: String!
    body: String
  }

  type User{
    _id: ID!
    id: Int!
    name: String!
    email: String!
    createdAt: Float!
    updatedAt: Float
    ideas: [Idea]
    votes: [Vote]
    voteCount: Int
    comments: [Comment]
  }

  type Vote {
    _id: ID!
    userId: Int!
    user: User
    ideaId: Int!
    idea: Idea
    createdAt: Float!
  }

  input VoteInput {
    ideaId: Int!
    userId: Int!
  }

  type Query {
    ideas: [Idea]
    idea(id: Int!): Idea
    users: [User!]!
    user(id: Int!): User
  }

  type Mutation {
    register(name: String!, email: String!, password: String!): AuthResponse
    login(email: String!, password: String!): AuthResponse
    createIdea(input: IdeaCreateInput!): Idea
    updateIdea(id: Int!, input: IdeaUpdateInput!): Idea
    vote(input: VoteInput): Boolean!
    unvote(input: VoteInput): Boolean!
    createComment(input: CommentInput): Comment
  }
`;

module.exports = {
  typeDefs
}