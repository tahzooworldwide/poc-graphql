const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');


// JWT helper
const signJWT = (subject) => {
  const tokenOptions = {
    algorithm: process.env.JWT_ALGO,
    subject: String(subject),
    expiresIn: process.env.JWT_EXPIRE
  }
  return jwt.sign({}, process.env.JWT_SECRET, tokenOptions);
}


// Resolvers
const resolvers = {

  Comment: {
    user: async ({ userId }, _, { dataSources }) => {
      return await dataSources.user.getUser(userId);
    },
    idea: async ({ ideaId }, _, { dataSources }) => {
      return await dataSources.idea.getIdea(ideaId);
    }
  },

  Idea: {
    comments: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentsByIdea(id);
    },
    votes: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVotesForIdea(id);
    },
    voteCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVoteCountForIdea(id);
    },
    user: async ({ userId }, _, { dataSources }) => {
      return await dataSources.user.getUser(userId);
    },
  },

  User: {
    ideas: async ({ id }, _, { dataSources }) => {
      return await dataSources.idea.getIdeasByUser(id);
    },
    comments: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentsByUser(id);
    },
    votes: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVotesByUser(id);
    },
    voteCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVoteCountByUser(id);
    },
  },

  Vote: {
    user: async ({ userId }, _, { dataSources }) => {
      return await dataSources.user.getUser(userId);
    },
    idea: async ({ ideaId }, _, { dataSources }) => {
      return await dataSources.idea.getIdea(ideaId);
    }
  },

  Query: {
    users: async (_, __, { dataSources }) => {
      return await dataSources.user.getUsers();
    },
    user: async (_, { id }, { dataSources }) => {
      return await dataSources.user.getUser(id);
    },
    ideas: async (_, __, { dataSources }) => {
      return await dataSources.idea.getIdeas();
    },
    idea: async (_, { id }, { dataSources }) => {
      return await dataSources.idea.getIdea(id);
    }
  },

  Mutation: {
    async createIdea(_, { input: { userId, title, body } }, { dataSources }) {
      return await dataSources.idea.createIdea(userId, title, body);
    },
    async updateIdea(_, { id, input: { title, body } }, { dataSources }) {
      return await dataSources.idea.updateIdea(id, title, body);
    },
    async createComment(_, { input: { ideaId, userId, body } }, { dataSources }) {
      return await dataSources.comment.createComment(ideaId, userId, body);
    },
    async vote(_, { input: { ideaId, userId } }, { dataSources }) {
      return await dataSources.vote.vote(ideaId, userId);
    },
    async unvote(_, { input: { ideaId, userId } }, { dataSources }) {
      return await dataSources.vote.unvote(ideaId, userId);
    },
    async register(_, { name, email, password }, { dataSources }) {
      const exists = await dataSources.auth.exists(email);
      if (exists > 0) {
        return {
          ok: false,
          message: "A users already exists with the provided email."
        }
      }
      const hashedPassword = await bcrypt.hash(password, 10);
      const result = await dataSources.auth.register(name, email, hashedPassword);

      if (!result) {
        return {
          ok: false,
          message: "Registration Failed."
        }
      }

      const token = signJWT(result.id)

      return {
        ok: true,
        token
      }
    },
    async login(_, { email, password }, { dataSources }) {
      const result = await dataSources.auth.getUserByEmail(email);
      if (!result) {
        return {
          ok: false,
          message: "No user exists that matches the provided credentials."
        }
      }

      const matchesHash = await bcrypt.compare(password, result.password);
      if (!matchesHash) {
        return {
          ok: false,
          message: "No user exists that matches the provided credentials."
        }
      }

      const token = signJWT(result.id)

      return {
        ok: true,
        token,
        message: "Authentication Successful."
      }
    }
  }
};

module.exports = { resolvers }