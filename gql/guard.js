const { shield } = require("graphql-shield");
const { isGuest, isAuthenticated } = require("./authorization-gates");


const guard = shield({

  Query: {
    ideas: isAuthenticated,
    idea: isAuthenticated,
    users: isAuthenticated,
    user: isAuthenticated
  },

  Mutation: {
    login: isGuest,
    register: isGuest,
    createComment: isAuthenticated,
    createIdea: isAuthenticated,
    updateIdea: isAuthenticated,
    vote: isAuthenticated,
    unvote: isAuthenticated,
  }

});

module.exports = { guard };