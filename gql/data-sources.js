const { MongoDataSource } = require('apollo-datasource-mongodb');

class AuthDataSource extends MongoDataSource {

  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }

  async exists(email) {
    return await this.collection.count({ email });
  }

  async register(name, email, password) {
    const id = await this.nextId();
    const createdAt = (new Date()).getTime();
    const proposedRecord = {
      id,
      name,
      email,
      password,
      createdAt,
      updatedAt: null
    }
    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }

  async getUserByEmail(email) {
    return await this.collection.findOne({ email });
  }

}

class CommentDataSource extends MongoDataSource {
  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }
  async getIdeas() {
    return await this.collection.find({}).toArray();
  }
  async getCommentsByIdea(id) {
    const query = { ideaId: id };
    const opts = { sort: { createdAt: 1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getCommentsByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async createComment(ideaId, userId, body) {
    const id = await this.nextId();
    const proposedRecord = {
      id,
      ideaId,
      userId,
      body,
      createdAt: (new Date()).getTime()
    }

    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }
}

class IdeaDataSource extends MongoDataSource {
  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }
  async getIdeas() {
    return await this.collection.find({}).toArray();
  }
  async getIdea(id) {
    return await this.collection.findOne({ id: id });
  }
  async getIdeasByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async createIdea(userId, title, body) {
    const id = await this.nextId();
    const now = (new Date()).getTime();
    const proposedRecord = {
      id,
      userId,
      title,
      body,
      createdAt: now,
      updatedAt: now
    }

    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }
  async updateIdea(id, title, body) {
    const updatedAt = (new Date()).getTime();
    const result = await this.collection.findOneAndUpdate({ id }, { $set: { title, body, updatedAt } });
    return result.value;
  }
}

class UserDataSource extends MongoDataSource {
  async getUsers() {
    return await this.collection.find({}).toArray();
  }
  async getUser(id) {
    return await this.collection.findOne({ id: id });
  }
}

class VoteDataSource extends MongoDataSource {
  async getVotesForIdea(id) {
    const query = { ideaId: id };
    const opts = { sort: { createdAt: 1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getVoteCountForIdea(id) {
    const query = { ideaId: id };
    return await this.collection.count(query);
  }
  async getVotesByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getVoteCountByUser(id) {
    const query = { userId: id };
    return await this.collection.count(query);
  }
  async vote(ideaId, userId) {
    const count = await this.collection.count({ ideaId, userId });
    if (count > 0) {
      return false;
    }
    const propsedRecored = {
      ideaId,
      userId,
      createdAt: (new Date()).getTime()
    }
    const result = await this.collection.insertOne(propsedRecored);
    return result.insertedId ? true : false;
  }
  async unvote(ideaId, userId) {
    const count = this.collection.count({ ideaId, userId });
    if (count === 0) {
      return false;
    }
    const result = await this.collection.deleteOne({ ideaId, userId });
    return result.deletedCount === 1;
  }
}

module.exports = {
  AuthDataSource,
  CommentDataSource,
  IdeaDataSource,
  UserDataSource,
  VoteDataSource
};