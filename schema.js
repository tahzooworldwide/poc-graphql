  const SCHEMA = {

  user: {
    id: "Int",
    name: "String",
    email: "String",
    password: "String",
    active: "Boolean",
    createdAt: "Date",
    updatedAt: "Date",
  },

  idea: {
    id: "Int",
    userId: "Int",
    title: "String",
    body: "String",
    createdAt: "Date",
    updatedAt: "Date",
  },

  comment: {
    id: "Int",
    userId: "Int",
    ideaId: "Int",
    body: "String",
    createdAt: "Date"
  },

  vote: {
    userId: "Int",
    ideaId: "Int",
    createdAt: "Date",
  }
}