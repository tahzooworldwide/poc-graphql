const SCHEMA = {
  databases: [
    {
      name: process.env.MONGO_DATABASE_DATA,
      type: "mongodb",
      collections: [
        { name: "users"},
        { name: "ideas"},
        { name: "comments"},
        { name: "votes"},
      ]
    },
    {
      name: process.env.MONGO_DATABASE_LOGGING,
      type: "mongodb",
      collections: [
        { name: "client"},
      ]
    },
  ]
}

module.exports = SCHEMA;