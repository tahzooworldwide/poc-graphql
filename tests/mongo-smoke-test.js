const { MongoClient } = require('mongodb')

// Connection URL
const url = 'mongodb://localhost:27017'
const client = new MongoClient(url)

async function main() {
  await client.connect()
  return `Successfully connected to MongoDB @ ${url}`
}

main()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close())