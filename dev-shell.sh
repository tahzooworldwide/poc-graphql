tmux new-session \; \
  send-keys 'npm run pm2:monitor' C-m \; \
  split-window -h \; \
  send-keys 'npm run pm2:logs' C-m \; \
  select-pane -t 0 \; \
  split-window -v -p 70 \; \
  