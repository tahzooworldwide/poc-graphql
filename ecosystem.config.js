require('dotenv').config();

module.exports = {
  apps: [
    {
      name: 'gql',
      script: 'gql/server.js',
      watch: ['./gql/'],
      env: {
        PORT: process.env.USERS_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        USERS_COLLECTION: process.env.USERS_COLLECTION,
        IDEAS_COLLECTION: process.env.IDEAS_COLLECTION,
        COMMENTS_COLLECTION: process.env.COMMENTS_COLLECTION,
        VOTES_COLLECTION: process.env.VOTES_COLLECTION,
        AUTH_COLLECTION: process.env.AUTH_COLLECTION,
        JWT_ALGO: process.env.JWT_ALGO,
        JWT_SECRET: process.env.JWT_SECRET,
        JWT_EXPIRE: process.env.JWT_EXPIRE,
      }
    }
  ],
};
